class CreateLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :locations do |t|
    	t.belongs_to :office, foreign_key: true, index: true
  	 	t.string 	:address_line_1
  	 	t.string 	:address_line_2
  	 	t.string 	:town
  	 	t.string 	:city
  	 	t.string 	:postcode, index: true
  	 	t.decimal	:latitude, precision: 10, scale: 6
  	 	t.decimal	:longitude, precision: 10, scale: 6
      
      t.timestamps
    end
  end
end
