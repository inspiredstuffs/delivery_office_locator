# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version - 2.4.2

* Rails version - 5.1.5

* System dependencies
    - Core GemSet Files used:
        * postcodes_io: https://github.com/jamesruston/postcodes_io - Postcode IO Wrapper.
        * geokit-rails: https://github.com/geokit/geokit-rails I have used this powerful GEM personally a couple of times. The ActiveRecord model is mapable to the acts_as_mappable wrapper, which gives it more life! and makes it so easy to work on realtime Spatial Calculations.
        * bootstrap: For the fluid and responsible Interface.  
        -  TESTING
                *rspec-rails
                *rails-controller-testing
                *factory_girl_rails
                *shoulda-matchers
* Configuration
I have initialized the postcode.io gem to a constant called *PIO* in config/initializers/postcodes_io.rb. Should incase you are wondering what is PIO.

  ## One very important thing to note is that I've used PostcodeIO gem to lookup postcodes i.e. postcode to geolocation coordinates. WHILE I used the geo-kit-rails gem for spartial calculations. e.g. I mapped and used it on the Location model for calculating or fetching locations based on *Distance within a certain radius, distance from or To a given co-ordinates* etc.


* Database creation
    *I created a table for offices and I also created a table for locations. To cover the possibilities of Office having multiple locations.
* Database initialization
    ### I love postgres!! although I use more of MYSQL in more current job role. :(
So I used Postgres for Database. Please feel free to change *database.yml* file to your prefered configurations.
   > and finally  Run rake db:create
* How to run the test suite
As mentioned above I have used Rspec for testing this app.
I wrote some specs covering the models and controllers only.
Specs can be located in:
    * spec/models
    * spec/controllers
 Each Model specs tests if the app has the right associations.
Each Controller Specs test if the expected page or data got rendered.
    #### The most important controller test is the home controller which does all the check as per the basic requirement of this app as stated below: 
> Given an input postcode and a radius (in miles), returns delivery offices which sit inside the radius ordered by distance (as the crow flies) Displays the distance (as the crow flies) of each delivery office from the input postcode
> Please configure spec/factories/locations.rb *postcode* to be in same radius as the postcode param specified in the spec/controllers/home_controller_spec.rb test to get a successful test. 
    
* Services (job queues, cache servers, search engines, etc.)
NONE
* Deployment instructions
Life will be made much easier if we all try RVM:
    * Install RVM: https://rvm.io/rvm/install
    * $: cd into project:
    * $: gem install bundler
    * $: bundle install
    * $: cd out and back into the app directory
    * $: rake db:create && rake db:migrate && rake db:seed
    * $: rails s
    * Congrats you got it working!
I already created some db:seeds for some Offices and associate Locations.
   ### You can add/edit/remove the Office and Locations in the front-end.

* ...
