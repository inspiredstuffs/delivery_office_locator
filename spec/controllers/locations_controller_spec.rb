require 'rails_helper'

RSpec.describe LocationsController, type: :controller do
  describe "GET index" do
  	before { @office = FactoryGirl.create :office }
    it "renders the index template" do
      get :index, params: { office_id: @office.id }
      expect(response).to render_template("index")
    end       
  end
end
