require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  before { @office = FactoryGirl.create :office }
  before { @location = FactoryGirl.create :location }
  describe "GET index" do
    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
    it "returns delivery offices which sit inside the radius ordered by 
        distance (as the crow flies) Displays the distance (as the crow flies) of 
        each delivery office from the input postcode " do
      get :locations_list, params: { query_data: {postcode: "M14 0ER", radius: 5}, draw: 1}
      lookup = PIO.lookup("M14 0ER")
      @location.distance = @location.distance_from([lookup.latitude, lookup.longitude])
      json = JSON.parse(response.body)
      data = Array.new
      tmpdata = Array.new
      tmpdata.push(@location.office.name.to_s.titleize)
      tmpdata.push(@location.full_address)
      tmpdata.push(@location.distance.round(1))
      data.push(tmpdata)
      expect(json["data"][0][3]).to eq(data[3]) ###MATCH THE POSTCODES
    end 
    it "Just some added functionality. Using client side javascript navigator plugin to detect
        User's current locations and to fetch office locations within set radius of the current geolocations coordinates" do
      lookup = PIO.lookup("M14 0ER")
      get :locations_list, params: { query_data: {latitude: lookup.latitude, longitude: lookup.longitude, radius: 5}, draw: 1}
      @location.distance = @location.distance_from([lookup.latitude, lookup.longitude])
      json = JSON.parse(response.body)
      data = Array.new
      tmpdata = Array.new
      tmpdata.push(@location.office.name.to_s.titleize)
      tmpdata.push(@location.full_address)
      tmpdata.push(@location.distance.round(1))
      data.push(tmpdata)
      expect(json["data"][0][3]).to eq(data[3]) ###MATCH THE POSTCODES
    end            
  end
end