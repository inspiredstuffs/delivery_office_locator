require 'rails_helper'

RSpec.describe Location, type: :model do	
	it { is_expected.to belong_to(:office) }
	it { is_expected.to validate_presence_of(:address_line_1) }
	it { is_expected.to validate_presence_of(:city) }
	it { is_expected.to validate_presence_of(:postcode) }
	it { is_expected.to validate_uniqueness_of(:postcode) }	
end
