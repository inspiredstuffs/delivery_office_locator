FactoryGirl.define do
  factory :location do
    address_line_1 "10 jonathan Street"
    city "London"
    postcode "M15 4AR"
    office factory: :office
  end
end
