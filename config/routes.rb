Rails.application.routes.draw do
	get "/locations_list", to: "home#locations_list"

  	resources :offices do
		resources :locations
	end

  	root "home#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
