class HomeController < ApplicationController

	def index
		@postcode = nil
	end

	def locations_list
		@postcode = params[:query_data][:postcode]
		if !@postcode.blank?
			lookup = PIO.lookup(@postcode)
		elsif !params[:query_data][:latitude].blank? && !params[:query_data][:longitude].blank?
			postcodes = PIO.reverse_geocode(longitude: params[:query_data][:longitude].to_f, latitude: params[:query_data][:latitude].to_f)
			@postcode = postcodes.last.postcode
			lookup = PIO.lookup(@postcode)
		else
			lookup = {}	
		end
		
		if !lookup.try(:info).blank?
			start = params[:start].blank? ? 0 : params[:start].to_i
			length = params[:length].blank? ? 10 : params[:length].to_i
			@total_locations = Location.offices_within(lookup.latitude, lookup.longitude, params[:query_data][:radius])
	    @locations = @total_locations[start, length]
	    data = Array.new
 			@locations.each do |loc|
	      tmpdata = Array.new
	      #tmpdata.push(loc.id)
	      tmpdata.push(loc.office.name.to_s.titleize)
	      tmpdata.push(loc.full_address)
	      tmpdata.push(loc.distance.round(1))
	      data.push(tmpdata)
      end	      
      	render json: {draw: params[:draw], recordsTotal: @total_locations.count, recordsFiltered: @total_locations.count, data: data, postcode: @postcode}    		
		else
			render json: {draw: params[:draw], recordsTotal: 0, recordsFiltered: 0, data: []}
		end    	
	end

end