// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap
//= require popper
//= require jquery_ujs
//= require vendor/bootstrap-slider.min
//= require vendor/jquery.dataTables.min
//= require vendor/dataTables.bootstrap4.min
//= require turbolinks
// require_tree .

var editHtml = function() {
    return '<div class="box box-primary"><div class="box-header with-border"><h3 class="box-title">Please Wait...</h3></div></div>';
};

var pleaseWait = function() {
    return '<div id="error-edit-inner-div" class="alert alert-success">Please Wait...</div>';
};

var showLoader = function() {
  $('#mask').show();$('#progress').show();
};

var hideLoader = function() {
  $('#mask').hide();$('#progress').hide();
};

var ajaxSubmit = function() {
  $( "form" ).submit(function( ) {
    $('.add-crud-edit-errors').html()
     showLoader();
  });
};

function getPosition(position){
  $("#latitude").val(position.coords.latitude);
  $("#longitude").val(position.coords.longitude);
  location_list_table.draw();
}

$(function (){

  if (navigator.geolocation){
    navigator.geolocation.getCurrentPosition(getPosition);
  } else {
    alert("Geolocation is not supported by this browser. Please try another browser to continue");
  } 

var queryData = function() {
    return {
        postcode: $("#postcode").val(),
        radius: $("#radius").val(),
        latitude: $("#latitude").val(),
        longitude: $("#longitude").val()
    }
};

 location_list_table = $('#locations_list').DataTable({
        processing: true,
        serverSide: true,
        bPaginate: true,
        bStateSave: true,
        ajax: { "url": '/locations_list',
            "data": function(d) {
                return $.extend( {}, d, {
                    query_data: queryData()
                })
            }},
        bLengthChange: false,
        dom: "rltip",
        ordering: false,
        createdRow: function (row, data, dataIndex) {
            $(row).attr('data-id', data[0]);
        }
    }).on('xhr.dt', function ( e, settings, json, xhr ) {
      if(json && json["postcode"])
        $("#postcode").val(json["postcode"]);
    });


  $(".dynamic-modal-trigger").on("click", function() {

      $.ajax({
          type: 'GET',
          url: $(this).data('url'),
          data: {
   
          },
          beforeSend: function (data) {
              $('#modal-content').html(editHtml());
          }
      });
  });

  if($('#postcode'))
  $('#postcode').change(function() {
    location_list_table.draw();
  });

  if($('#radius'))
  radius_slider = $('#radius').slider({
    formatter: function(value) {
      $("#distance" ).text(value);
    }
  })

  if(radius_slider)
  radius_slider.on('slideStop', function(ev){
     location_list_table.draw();
  });

});