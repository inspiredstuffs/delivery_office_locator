class Location < ApplicationRecord
	belongs_to :office
	attr_accessor :distance
  	acts_as_mappable :default_units => :miles, #As You Can see here my default unit is in miles
                   :default_formula => :sphere, #:sphere Haversine calculations instead of :flat which gives gives the Pythagoreum Theory
                   :distance_field_name => :distance,
                   :lat_column_name => :latitude,
                   :lng_column_name => :longitude
	
	validates_presence_of :address_line_1, :city, :postcode
	validates_uniqueness_of :postcode
	validate :validate_postcode

	def full_address
		[address_line_1, town, city, postcode].compact.join(", ")
	end

	def validate_postcode # 
		lookup = PIO.lookup(postcode.to_s)
		if lookup.try(:info).blank?
			errors.add(:postcode, "doesn't exist!")
		else
			self.longitude = lookup.longitude
			self.latitude = lookup.latitude
		end
	end


  def self.offices_within(latitude, longitude, radius) #The main heart of everything. We can also search in range of radius not less that 1 and not more than 2 miles like Location.within([1..2], origin)
    return self.within(radius, :origin => [latitude, longitude]).all
    .each{|a| a.distance = a.distance_from([latitude, longitude])} #I addedd attr_accessor distance (This is the distance_from the given postcode to the office location) I need to use the distance to sort and as well in the view
    .sort {|a,b| a.distance <=> b.distance }  
  end

end
